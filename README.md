# udev-usb-rule-helper

This is a simple shell script to create a custom udev rule for USB devices.

## Prerequisites:
  * sed
  * awk
  * fzf

## Usage:
  1. Plug in the device you want to create a rule for.
  2. Run the udev-usb-rule-helper script.
  3. Select the device from the list with the cursor keys (or typing parts of the entry) and select by pressing enter.
  4. Next you will be presented with a list of the attributes that udev sees of your device. You can select multiple attributes by marking them with the tab key. If the order of the attributes matters to you select them in the order you want them to appear in the rule. If you selected everything you want press enter to continue.
  5. Now you are presented with a list of possible udev action templates. Select those that you want to use like in the last step and press enter.
  6. Copy the line printed to your terminal to a text editor and fill in the template values.
  7. Save the file under a name that fits the other entries in your distributions /etc/udev/rules.d directory. Usually this takes the form nn-description.rules, where nn stands for a number which signifies the order in which udev reads the rules in that directory.
  8. Copy that file to /etc/udev/rules.d and set the file attributes to the owner, group, permisions etc. like the other files found in there (e.g. sudo chown root:root nn-description.rules; sudo chmod 644 nn-description.rules)
  9. Reload the udev rules with the command: udevadm control --reload
